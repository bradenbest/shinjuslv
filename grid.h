#ifndef GRID_H
#define GRID_H

#define GRIDW 9

#define grid_xy_is_number(x, y) \
    grid_cell_is_number(grid_get_cell(x, y))

#define grid_xy_get_number(x, y) \
    grid_cell_get_number(grid_get_cell(x, y))

struct coord {
    int x;
    int y;
};

struct rect {
    int x0;
    int y0;
    int x1;
    int y1;
};

enum grid_value {
    GRID_BLANK,
    GRID_UNKNOWN,
    GRID_RULEDOUT,
    GRID_SOLUTION,
};

void         grid_init                            (int const *offsets, size_t len);
void         grid_show_solution                   (void);
int          grid_get_solution_distance_from_str  (char const *coord, size_t len);
int const *  grid_get_ring                        (int offset);
int          grid_update_invalid                  (void);
void         grid_draw                            (void);
int *        grid_get_cell                        (int x, int y);
int *        grid_get_cell_from_str               (char const *coordstr, size_t len);
int          grid_cell_is_number                  (int const *cell);
int          grid_cell_get_number                 (int const *cell);
void         grid_cell_set_number                 (int *cell, int value);

#endif //GRID_H
