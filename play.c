#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "grid.h"
#include "bradenlib.h"

static int moves = 4;

static inline void
usage(void)
{
    static char const *usage_text[] = {
        "play - play a game of shinju",
        "usage: play <difficulty>",
        "difficulty: number from 1-8",
        "",
        "each difficulty step adds 10 shells",
        "example: play 1",
        "",
        "  0 1 2 3 4 5 6 7 8",
        "A ? . ? . . . . . ?",
        "B ? . . . . ? . . .",
        "C . . . . . . . . .",
        "D . ? . . . . . . .",
        "E . . . . . ? . . .",
        "F . . . . . . . . .",
        "G . . . ? . . . . .",
        "H . . . . . . . . .",
        "I . . ? . . . ? . .",
        "",
        "symbols:",
        "  .   blank square",
        "  1-8 hint",
        "  ?   possible answer",
        "",
        "To play the game, enter a cell to flip and use the hint to choose",
        "another cell. You get 4 flips. Find the target and win.",
        NULL
    };

    putsv(usage_text);
    exit(1);
}

static inline void
show_prompt(void)
{
    static char const *lines[] = {
        "Enter your guess as a coordinate.",
        "c4, C4 or 22 all refer to the same cell.",
        NULL
    };

    putsv(lines);
    printf("Moves left: %u\n", moves);
    fputs("> ", stdout);
}

// generate { 0 .. GRIDW * GRIDW }
// fisher yates shuffle
// send subset (difficulty) to grid_init
static inline void
generate_random(int difficulty)
{
    int offsets[GRIDW * GRIDW];

    for (size_t i = 0; i < GRIDW * GRIDW; ++i)
        offsets[i] = i;

    fyshuffle(offsets, GRIDW * GRIDW, sizeof *offsets);
    grid_init(offsets, difficulty * 10);
}

static inline int
parse_input(char const *buf, size_t len)
{
    int *cell = grid_get_cell_from_str(buf, len);
    int distance;

    if (grid_cell_is_number(cell)) {
        puts("Cell is already flipped.");
        return 1;
    }

    if (*cell == GRID_BLANK) {
        puts("Cell is blank");
        return 1;
    }

    distance = grid_get_solution_distance_from_str(buf, len);

    if (distance == 0) {
        puts("You win!");
        return 0;
    }

    if (--moves == 0) {
        puts("Out of moves.");
        grid_show_solution();
        return 0;
    }

    grid_cell_set_number(cell, distance);
    return 1;
}

static inline int
run_repl(void)
{
    char linebuf[16];
    size_t nread;

    grid_draw();
    show_prompt();

    if ((nread = get_line(linebuf, 16)) == 0) {
        if (feof(stdin))
            puts("^D\nGoodbye.");

        return 0;
    }

    return parse_input(linebuf, nread);
}

int
main(int argc, char **argv)
{
    int difficulty;

    if (argc == 1)
        usage();

    difficulty = strntol(argv[1], strlen(argv[1]), 10);

    if (difficulty < 1 || difficulty > 8)
        usage();

    srand(time(NULL));

    generate_random(difficulty);

    while(run_repl())
        ;

    return 0;
}
