// Generated from bradenlib
// https://gitlab.com/bradenbest/bradenlib
// Command: make fyshuffle putsv strlen_delim ivfind strntol get_line
/*
 * Name:
 *     fyshuffle v1.0
 *
 * Synopsis:
 *     void  fyshuffle  (void *base, size_t nmemb, size_t size);
 *
 * Arguments:
 *     base:  base of memory to be shuffled
 *     nmemb: number of members
 *     size:  size of a member
 *
 * Description:
 *     Performs a Fisher-Yates shuffle using rand(3) on base. The
 *     arguments are arranged after qsort(3) rather than fwrite(3). I
 *     know, the libc functions aren't very consistent in their
 *     arguments.
 */
#ifndef BRADENLIB_FYSHUFFLE_H
#define BRADENLIB_FYSHUFFLE_H

#include <string.h>
#include <stdlib.h>

static inline void
fyshuffle(void *base, size_t nmemb, size_t size)
{
    char temp[size];

    for (size_t i = 0; i < nmemb - 1; ++i) {
        size_t randoffset = rand() % (nmemb - i);
        char *a = base + size * i;
        char *b = base + size * (i + randoffset);

        if (a == b)
            continue;

        memcpy(temp, a, size);
        memcpy(a, b, size);
        memcpy(b, temp, size);
    }
}

#endif // BRADENLIB_FYSHUFFLE_H
/*
 * Name:
 *     putsv v1.0
 *
 * Synopsis:
 *     int  putsv_implementation  (char const **lines, FILE *out, int flags);
 *
 *     #define  putsv   (lines)
 *     #define  fputsv  (lines, file)
 *
 * Arguments:
 *     lines: array of strings terminated by NULL
 *     out:   file to write to
 *     flags: bitmask
 *
 * Flags:
 *     PUTSV_FLAG_USE_PUTS: if set, use puts, else use fputs.
 *     more specifically, this causes newlines to be appended to each
 *     line and ignores the file in favor of stdout
 *
 * Description:
 *     Prints every string in lines on their own line. Useful for
 *     printing a bunch of lines of text without having to call puts a
 *     million times. sv stands for string vector.
 *
 * Return Value:
 *     Returns the number of characters written, or EOF on error. This is
 *     done in the name of consistency with puts(3).
 *
 * Example:
 *     void usage(void){
 *         static char const *usage_text[] = {
 *             "program - does a thing",
 *             "usage: program foo bar [options]",
 *             "options:",
 *             "  -h  show this help",
 *             "  -q  quiet output",
 *             NULL
 *         };
 *
 *         putsv(usage_text);
 *         exit(1);
 *     }
 */
#ifndef BRADENLIB_PUTSV_H
#define BRADENLIB_PUTSV_H

#include <stdio.h>

#define PUTSV_FLAG_USE_PUTS 0x1

#define putsv(lines) \
    putsv_implementation(lines, stdout, PUTSV_FLAG_USE_PUTS)

#define fputsv(lines, file) \
    putsv_implementation(lines, file, 0)

/*
 * 1. out is ignored if PUTSV_FLAG_USE_PUTS is set. This void expression
 *    should prevent compilers from warning about unused variables
 */
static inline int
putsv_implementation(char const **lines, FILE *out, int flags)
{
    (void)out; // 1
    int total = 0;
    int retval;

    for (int i = 0; lines[i] != NULL; ++i) {
        if (flags & PUTSV_FLAG_USE_PUTS)
            retval = puts(lines[i]);
        else
            retval = fputs(lines[i], out);

        if (retval == EOF)
            return EOF;

        total += retval;
    }

    return total;
}

#endif // BRADENLIB_PUTSV_H
/*
 * Name:
 *     strlen_delim v1.0
 *
 * Synopsis:
 *     size_t  strlen_delim  (char const *str, char delim);
 *
 * Arguments:
 *     str:   string
 *     delim: value that marks end of string
 *
 * Description:
 *     same as strlen(3) but using a different delimeter than strlen.
 *     strlen_delim(str, '\0') is the same as strlen(str).
 *
 *     Use when strtok(3) is inappropriate
 *
 * Return Value:
 *     The length of the string not including the delimeter
 */
#ifndef BRADENLIB_STRLEN_DELIM_H
#define BRADENLIB_STRLEN_DELIM_H

#include <stddef.h>

static inline size_t
strlen_delim(char const *str, char delim)
{
    size_t length = 0;

    while (str[length] != delim)
        ++length;

    return length;
}

#endif // BRADENLIB_STRLEN_DELIM_H
/*
 * Name:
 *     ivfind v1.0
 *
 * Synopsis:
 *     int const *  ivfind        (int const *iv, size_t len, int value);
 *     int const *  ivfind_delim  (int const *iv, int delim, int value);
 *
 * Arguments:
 *     iv:    int vector to search
 *     len:   length of vector
 *     value: value to search for
 *     delim: delimeter to terminate on
 *
 * Description:
 *     Finds the location of the first element in iv that matches value.
 *
 *     ivfind_delim is the same, except it takes a delimeter instead of
 *     a length
 *
 *     Similar in concept to strchr(3).
 *
 * Return Value:
 *     Returns pointer to location or NULL if no match is found.
 */
#ifndef BRADENLIB_IVFIND_H
#define BRADENLIB_IVFIND_H

#include <stddef.h>

static inline int const *
ivfind(int const *iv, size_t len, int value)
{
    for (size_t i = 0; i < len; ++i)
        if (iv[i] == value)
            return iv + i;

    return NULL;
}

static inline int const *
ivfind_delim(int const *iv, int delim, int value)
{
    for (size_t i = 0; iv[i] != delim; ++i)
        if (iv[i] == value)
            return iv + i;

    return NULL;
}

#endif // BRADENLIB_IVFIND_H
/*
 * Name:
 *     strntol v1.0
 *
 * Synopsis:
 *     long  strntol  (char const *str, size_t len, int radix);
 *
 * Arguments:
 *     str:   string to convert
 *     len:   length of string
 *     radix: the radix base (max 36)
 *
 * Description:
 *     Similar to atoi(3) and strtol(3), except it takes a length
 *     argument. Useful for non-zero-terminated strings.
 *
 *     Specifically, strntoi discards leading whitespace (as defined by
 *     isspace) and checks the first non-whitespace character for -/+
 *     and adjusts the sign accordingly, and then parses digits in base
 *     (radix) until the first non-digit character, so "   -42abc"
 *     will parse as -42
 *
 *     Note that although it has `str` in the name, there is no check
 *     for a zero byte. The fact that a null byte appearing before the
 *     end of the string causes the loop to terminate is merely emergent
 *     behavior that happens to be convenient.
 *
 * Return Value:
 *     returns the parsed number
 *
 */
#ifndef BRADENLIB_STRNTOL_H
#define BRADENLIB_STRNTOL_H

#include <string.h>
#include <ctype.h>

static inline long
strntol(char const *str, size_t len, int radix)
{
    static char const *radixchr = "0123456789ABCDEFGHIJKMLNOPQRSTUVWXYZ";
    long total = 0;
    int sign = 1;

    while (len > 0 && isspace(*str)) {
        ++str;
        --len;
    }

    if (len == 0)
        return 0;

    if (*str == '+'){
        ++str;
        --len;
    }

    if (*str == '-') {
        ++str;
        --len;
        sign = -1;
    }

    for (size_t i = 0; i < len; ++i) {
        char const *res = memchr(radixchr, toupper(str[i]), radix);

        if (res == NULL)
            break;

        total = (total * radix) + (res - radixchr);
    }

    return total * sign;
}

#endif // BRADENLIB_STRNTOL_H
/*
 * Name:
 *     get_line v1.0
 *
 * Synopsis:
 *     size_t  get_line_implementation  (char *buffer, size_t size, FILE *file, int flags);
 *
 *     #define  get_line              (buffer, size)
 *     #define  get_line_from         (buffer, size, file)
 *     #define  get_line_greedy       (buffer, size)
 *     #define  get_line_from_greedy  (buffer, size, file)
 *     #define  get_line_discard      ()
 *
 * Arguments:
 *     buffer: buffer where input will go
 *     size:   size of buffer
 *     file:   file to read from
 *     flags:  bitmask
 *
 * Flags:
 *     GET_LINE_FLAG_DISCARD_TRAILING: if this flag is set, the function
 *     will continue to consume the line when the buffer runs out of
 *     space. Otherwise, it will exit immediately.
 *
 * Description:
 *     `get_line_implementation` gets a line of input from file
 *     excluding the newline.
 *
 *     `get_line` forces GET_LINE_FLAG_DISCARD_TRAILING and sets file to
 *     stdin.
 *
 *     `get_line_from` and `get_line_from_greedy` take a file arguemnt.
 *
 *     `get_line_greedy` and `get_line_from_greedy` turn off the
 *     GET_LINE_FLAG_DISCARD_TRAILING flag.
 *
 *     `get_line_discard` reads a line of input and discards it.
 *
 *     If buffer is NULL and bufsz is not 0, the behavior is undefined.
 *     Otherwise, the function will consume the entire line of input or
 *     immediately exit depending on GET_LINE_FLAG_DISCARD_TRAILING. See
 *     `get_line_discard`. This is useful for 'press enter to continue'
 *     prompts.
 *
 *     If the buffer runs out of space before a newline is encountered
 *     and the GET_LINE_FLAG_DISCARD_TRAILING is set, the function
 *     consumes the rest of the line.
 *
 * Return Value:
 *     Returns the number of characters read (excluding the newline) or
 *     0. There is no distinction between 0 bytes read and EOF. To check
 *     for EOF, use feof(file).
 */

#ifndef BRADENLIB_GET_LINE_H
#define BRADENLIB_GET_LINE_H

#include <stdio.h>

#define GET_LINE_FLAG_DISCARD_TRAILING 0x1

#define get_line_from(buffer, bufsz, file) \
    get_line_implementation(buffer, bufsz, file, GET_LINE_FLAG_DISCARD_TRAILING)

#define get_line_from_greedy(buffer, bufsz, file) \
    get_line_implementation(buffer, bufsz, file, 0x0)

#define get_line(buffer, bufsz) \
    get_line_from(buffer, bufsz, stdin)

#define get_line_greedy(buffer, bufsz) \
    get_line_from_greedy(buffer, bufsz, stdin)

#define get_line_discard() \
    get_line(NULL, 0)

/*
 * 1. the loop conditions are a little complicated, so I broke it up into
 *    two parts. The first part will fill the buffer while there is space
 *    unless zero characters are read or the character read is a newline
 *
 * 2. the second part checks that there is no space in the buffer AND the
 *    DISCARD_TRAILING flag is set. If both of these conditions are true,
 *    then the rest of the line is consumed.
 *
 * If refactoring this function, be sure that the behavior complies with
 * the description at the top of the file. Specifically...
 *
 *  - greedy mode (DISCARD_TRAILING flag not set) MUST read everything
 *    (except newline)
 *  - normal mode MUST discard the trailoff
 *  - discard mode MUST discard everything.
 *
 *  Use tget_line.c to test this function.
 *
 *  An example of a refactoring introducing a bug would be if the fread
 *  is moved into the while header. Doing this would always read a
 *  character, which would result in a character getting lost in greedy
 *  mode. (size 10 input "Hello World!\n" -> {"Hello Worl", 10}, {"!", 1})
 */
static inline size_t
get_line_implementation(char *buffer, size_t bufsz, FILE *in, int flags)
{
    static char ch;
    size_t nread;
    size_t buflen = 0;

    if (in == NULL || feof(in))
        goto exit_eof;

    if (ferror(in))
        goto exit_error;

    while (buflen < bufsz) { // 1
        if ((nread = fread(&ch, sizeof ch, 1, in)) == 0 || ch == '\n')
            break;

        buffer[buflen++] = ch;
    }

    if (buflen >= bufsz && (flags & GET_LINE_FLAG_DISCARD_TRAILING)) // 2
        while ((nread = fread(&ch, sizeof ch, 1, in)) > 0 && ch != '\n')
            ;

    return buflen;

exit_error:
    {
        printf("get_line_implementation: Encountered I/O error.\n");
        return 0;
    }

exit_eof:
    return 0;
}

#endif // BRADENLIB_GET_LINE_H
