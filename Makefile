CFLAGS = -Wall -Wextra -O3

all: shinjuslv play

shinjuslv: solver.o grid.o
	$(CC) $^ -o $@

play: play.o grid.o
	$(CC) $^ -o $@

clean:
	rm -f *.o
	rm -f shinjuslv play

.PHONY: clean all
