#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include "grid.h"
#include "bradenlib.h"

#define GRID2D(x, y) \
    ( (GRIDW * (y)) + (x) )

#define GRID1D(off) \
    (off) % GRIDW, (off) / GRIDW

#define GRIDN_MASK 0x10

static inline int  strtocoord  (char const *coordstr, size_t len);

static int grid[GRIDW * GRIDW];
static int solution;
static char const *rowdelim = "ABCDEFGHI";
static char const *gridvalue_chr = ".?x@";

static inline int
strtocoord(char const *coordstr, size_t len)
{
    char const *res = strchr(rowdelim, toupper(coordstr[0]));

    if (res == NULL)
        return (int)strntol(coordstr, len, 10);

    return GRID2D((int)strntol(coordstr + 1, len - 1, 10), res - rowdelim);
}

/* takes a list of offsets and sets them to unknown
 * chooses a random one of them and marks it as the solution
 * this second part is only relevant to the `play` program
 */
inline void
grid_init(int const *offsets, size_t len)
{
    for (size_t i = 0; i < len; ++i)
        grid[offsets[i]] = GRID_UNKNOWN;

    solution = offsets[rand() % len];
}

inline void
grid_show_solution(void)
{
    grid[solution] = GRID_SOLUTION;
    grid_draw();
}

inline int
grid_get_solution_distance_from_str(char const *coord, size_t len)
{
    int offset = strtocoord(coord, len);
    struct coord cguess = {GRID1D(offset)};
    struct coord csolution = {GRID1D(solution)};
    struct coord cdiff = {
        cguess.x - csolution.x,
        cguess.y - csolution.y
    };

    if (cdiff.x < 0)
        cdiff.x *= -1;

    if (cdiff.y < 0)
        cdiff.y *= -1;

    return (cdiff.x > cdiff.y) ? (cdiff.x) : (cdiff.y);
}

/* gets a filtered ring around cell at offset
 * ring only includes unknown values, as those are candidates
 */
inline int const *
grid_get_ring(int offset)
{
    static int ring[64];
    int *ringp = ring;
    struct coord c2d = { GRID1D(offset) };
    int value = grid[offset] & (GRIDN_MASK - 1);
    struct rect boundary = {
        c2d.x - value,
        c2d.y - value,
        c2d.x + value,
        c2d.y + value,
    };

    for (int y = boundary.y0; y <= boundary.y1; ++y) {
        for (int x = boundary.x0; x <= boundary.x1; ++x) {
            int cellvalue;

            if (x > boundary.x0 && x < boundary.x1 && y > boundary.y0 && y < boundary.y1)
                continue;

            if (x < 0 || y < 0 || x >= GRIDW || y >= GRIDW)
                continue;

            cellvalue = grid[GRID2D(x, y)];

            if (cellvalue == GRID_UNKNOWN)
                *ringp++ = GRID2D(x, y);
        }
    }

    *ringp = -1;
    return ring;
}

/* used in solver to rule out cells, state that the game is unsolveable
 * or state the solution.
 */
inline int
grid_update_invalid(void)
{
    size_t valid_len = GRIDW * GRIDW;
    int valid_idx[valid_len];
    int *validp = valid_idx;
    int const *ring;

    for (size_t i = 0; i < valid_len; ++i) {
        if (grid[i] == GRID_RULEDOUT || grid[i] == GRID_BLANK)
            continue;

        if (grid[i] & GRIDN_MASK)
            continue;

        *validp++ = i;
    }

    valid_len = validp - valid_idx;

    for (int i = 0; i < GRIDW * GRIDW; ++i) {
        int backbuf[valid_len];
        int *backp = backbuf;

        if (!(grid[i] & GRIDN_MASK))
            continue;

        ring = grid_get_ring(i);

        for (size_t j = 0; j < valid_len; ++j)
            if (ivfind_delim(ring, -1, valid_idx[j]))
                *backp++ = valid_idx[j];

        valid_len = backp - backbuf;
        memcpy(valid_idx, backbuf, valid_len * sizeof *backbuf);
    }

    if (valid_len == 0) {
        puts("Warning: puzzle is impossible. No candidates left.");
        return 0;
    }

    for (int i = 0; i < GRIDW * GRIDW; ++i) {
        if (ivfind(valid_idx, valid_len, i) != NULL)
            continue;

        if (grid[i] == GRID_UNKNOWN)
            grid[i] = GRID_RULEDOUT;
    }

    if (valid_len == 1) {
        struct coord candidate = { GRID1D(valid_idx[0]) };
        grid_draw();
        printf("Only remaining candidate is at %c%u. Quitting.\n", rowdelim[candidate.y], candidate.x);
        return 0;
    }

    return 1;
}

void
grid_draw(void)
{
    puts("  0 1 2 3 4 5 6 7 8");

    for (int y = 0; y < GRIDW; ++y) {
        printf("%c ", rowdelim[y]);

        for (int x = 0; x < GRIDW; ++x) {
            int *cell = grid_get_cell(x, y);

            if (grid_cell_is_number(cell))
                printf("%u ", grid_cell_get_number(cell));
            else
                printf("%c ", gridvalue_chr[*cell]);
        }

        putchar('\n');
    }
}

inline int *
grid_get_cell(int x, int y)
{
    return grid + GRID2D(x, y);
}

inline int *
grid_get_cell_from_str(char const *coordstr, size_t len)
{
    return grid + strtocoord(coordstr, len);
}

inline int
grid_cell_is_number(int const *cell)
{
    return !!(*cell & GRIDN_MASK);
}

inline int
grid_cell_get_number(int const *cell)
{
    return *cell & (GRIDN_MASK - 1);
}

inline void
grid_cell_set_number(int *cell, int value)
{
    *cell = GRIDN_MASK | value;
}
