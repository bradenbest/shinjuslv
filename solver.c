#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "bradenlib.h"
#include "grid.h"

static inline void
usage(void)
{
    static char const *usage_text[] = {
        "shinjuslv - solving aid for ninjakiwi's shinju",
        "usage: shinju <coords...>",
        "coords: list of coords (1d or 2d)",
        "1d coords are offsets from 0 to 81",
        "2d coords are pairs like H5",
        "",
        "example: shinjuslv 2 8 9 14",
        "example: shinjuslv A2 A8 B0 B5",
        "example: shinjuslv 2 8 B0 B5",
        "these all produce this grid:",
        "",
        "  0 1 2 3 4 5 6 7 8",
        "A . . ? . . . . . ?",
        "B ? . . . . ? . . .",
        "C . . . . . . . . .",
        "D . . . . . . . . .",
        "E . . . . . . . . .",
        "F . . . . . . . . .",
        "G . . . . . . . . .",
        "H . . . . . . . . .",
        "I . . . . . . . . .",
        "",
        "After you generate the grid, the program will become interactive",
        "and use your feedback to rule out wrong choices.",
        "",
        "symbols:",
        "  .  blank square",
        "  x  ruled out square",
        "  ?  possible answer",
        NULL
    };

    putsv(usage_text);
    exit(1);
}

static inline void
parse_args(char **argv)
{
    for (int i = 0; argv[i] != NULL; ++i)
        *grid_get_cell_from_str(argv[i], strlen(argv[i])) = GRID_UNKNOWN;
}

static inline int
parse_user_input(char const *buf, size_t len)
{
    size_t coordlen = strlen_delim(buf, ',');
    int *cell = grid_get_cell_from_str(buf, coordlen);
    int value = strntol(buf + coordlen + 1, len - (coordlen + 1), 10);

    if (grid_cell_is_number(cell))
        printf("Warning: cell at coordinate %.*s already has number value.\n", (int)coordlen, buf);

    if (*cell == GRID_RULEDOUT)
        printf("Warning: cell at coordinate %.*s is ruled out.\n", (int)coordlen, buf);

    if (*cell == GRID_BLANK) {
        printf("Warning: cell at coordinate %.*s is blank. Operation canceled.\n", (int)coordlen, buf);
        return 1;
    }

    grid_cell_set_number(cell, value);
    return grid_update_invalid();
}

static inline void
show_prompt(void)
{
    static char const *lines[] = {
        "Enter a coordinate and a value separated by a comma\n",
        "Example: f4,7 means that the shell at f4 has value 7\n",
        "> ",
        NULL
    };

    fputsv(lines, stdout);
}

static inline int
run_repl(void)
{
    static char linebuf[16];
    size_t nread;
    char const *match;

    grid_draw();
    show_prompt();

    if ((nread = get_line(linebuf, 16)) == 0) {
        if (feof(stdin))
            puts("^D\nGoodbye.");

        return 0;
    }

    if ((match = memchr(linebuf, ',', nread)) == NULL) {
        printf("Warning: `,` expected after '%.*s'.\n", (int)nread, linebuf);
        return 1;
    }

    return parse_user_input(linebuf, nread);
}

int
main(int argc, char **argv)
{
    if (argc == 1)
        usage();

    parse_args(argv + 1);

    while(run_repl())
        ;

    return 0;
}
