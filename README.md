## shinjuslv - a solving aid for ninjakiwi's game, shinju

## Build

Simply run make

    $ make

## Usage

First, the map needs to be loaded in via program arguments.

    $ ./shinjuslv a0 a3 a4 a8 b1 b2 c1 c2 d1 d3 d6 d8 e4 e7 f1 f3 f8 g4 g5 h0 h3 h5 h7 i1 i6 i8
      0 1 2 3 4 5 6 7 8
    A ? . . ? ? . . . ?
    B . ? ? . . . . . .
    C . ? ? . . . . . .
    D . ? . ? . . ? . ?
    E . . . . ? . . ? .
    F . ? . ? . . . . ?
    G . . . . ? ? . . .
    H ? . . ? . ? . ? .
    I . ? . . . . ? . ?
    Enter a coordinate and a value separated by a comma
    Example: f4,7 means that the shell at f4 has value 7
    >

Each argument represents a coordinate that is marked with `?`. This represents a shell. Coordinates can be 2D
or a number from 0..80. `C1`, `c1`, and `19` all refer to the same cell.

Once you've got the program running, it pretty much tells you how to play. The format is `coordinate,hint`. Let's say
the pearl is at I8. When you flip A0 in-game, the shell will show the number 8, so type `a0,8`. The program will then
use that hint to rule out shells that can't hold the pearl, and then update.

    > a0,8
      0 1 2 3 4 5 6 7 8
    A 8 . . x x . . . ?
    B . x x . . . . . .
    C . x x . . . . . .
    D . x . x . . x . ?
    E . . . . x . . x .
    F . x . x . . . . ?
    G . . . . x x . . .
    H x . . x . x . x .
    I . ? . . . . ? . ?
    Enter a coordinate and a value separated by a comma
    Example: f4,7 means that the shell at f4 has value 7
    >

Cells marked with x have been ruled out. Now flip i1. The game says 7.

    > i1,7
      0 1 2 3 4 5 6 7 8
    A 8 . . x x . . . x
    B . x x . . . . . .
    C . x x . . . . . .
    D . x . x . . x . ?
    E . . . . x . . x .
    F . x . x . . . . ?
    G . . . . x x . . .
    H x . . x . x . x .
    I . 7 . . . . x . ?
    Enter a coordinate and a value separated by a comma
    Example: f4,7 means that the shell at f4 has value 7
    >

Now flip f8. Game says 3.

    > f8,3
      0 1 2 3 4 5 6 7 8
    A 8 . . x x . . . x
    B . x x . . . . . .
    C . x x . . . . . .
    D . x . x . . x . x
    E . . . . x . . x .
    F . x . x . . . . 3
    G . . . . x x . . .
    H x . . x . x . x .
    I . 7 . . . . x . ?
    Only remaining candidate is at I8. Quitting.

You now know how to use this program.

## Game

You can play the game.

    $ ./play 5
      0 1 2 3 4 5 6 7 8
    A ? . . . ? . ? . ?
    B ? ? ? ? ? ? ? . ?
    C ? . ? . . ? . ? ?
    D . ? . ? ? ? ? ? ?
    E ? . . . . ? ? . .
    F ? . ? . ? ? . ? .
    G ? ? . . ? ? . ? ?
    H ? ? ? ? . ? ? . ?
    I . . ? . ? . ? ? ?
    Enter your guess as a coordinate.
    c4, C4 or 22 all refer to the same cell.
    Moves left: 4
    >

The argument is the difficulty. On difficulty 5, there are 50 possible spaces where the target could be.
